project(
  'mmsdtng',
  'c', 'cpp',
  version : '2.6.0',
  meson_version : '>= 0.56.0',
)

check_headers = [
  ['HAVE_DLFCN_H', 'dlfcn.h'],
  ['HAVE_INTTYPES_H', 'inttypes.h'],
  ['HAVE_STDINT_H', 'stdint.h'],
  ['HAVE_STDIO_H', 'stdio.h'],
  ['HAVE_STDLIB_H', 'stdlib.h'],
  ['HAVE_STRINGS_H', 'strings.h'],
  ['HAVE_STRING_H', 'string.h'],
  ['HAVE_SYS_STAT_H', 'sys/stat.h'],
  ['HAVE_SYS_TYPES_H', 'sys/types.h'],
  ['HAVE_UNISTD_H', 'unistd.h']
]

cc = meson.get_compiler('c')
conf_data = configuration_data()

foreach h : check_headers
  if cc.has_header(h.get(1))
    conf_data.set(h.get(0), 1)
  endif
endforeach
conf_data.set('VERSION', meson.project_version())
conf_data.set('NAME', meson.project_name())
mobile_broadband_provider_info_database = dependency('mobile-broadband-provider-info').get_variable(pkgconfig: 'database')
conf_data.set('MOBILE_BROADBAND_PROVIDER_INFO_DATABASE', mobile_broadband_provider_info_database)
conf_data.set('SOURCE_ROOT', meson.project_source_root())

conf = configure_file(
  input : 'config.h.in',
  output : 'config.h',
  configuration : conf_data
)

add_project_arguments('-DMMS_PLUGIN_BUILTIN', language : 'c')
add_project_arguments('-DHAVE_CONFIG_H', language : 'c')
add_project_arguments('-DPLUGINDIR="@0@/mms/plugins"'.format(get_option('libdir')), language : 'c')

project_c_args = []
test_c_args = [
  '-Wcast-align',
  '-Wdeclaration-after-statement',
  '-Werror=address',
  '-Werror=array-bounds',
  '-Werror=empty-body',
  '-Werror=implicit',
  '-Werror=implicit-function-declaration',
  '-Werror=incompatible-pointer-types',
  '-Werror=init-self',
  '-Werror=int-conversion',
  '-Werror=int-to-pointer-cast',
  '-Werror=main',
  '-Werror=misleading-indentation',
  '-Werror=missing-braces',
  '-Werror=missing-include-dirs',
  '-Werror=nonnull',
  '-Werror=overflow',
  '-Werror=parenthesis',
  '-Werror=pointer-arith',
  '-Werror=pointer-to-int-cast',
  '-Werror=redundant-decls',
  '-Werror=return-type',
  '-Werror=sequence-point',
  '-Werror=shadow',
  '-Werror=strict-prototypes',
  '-Werror=trigraphs',
  '-Werror=undef',
  '-Werror=write-strings',
  '-Wformat-nonliteral',
  '-Wignored-qualifiers',
  '-Wimplicit-function-declaration',
  '-Wlogical-op',
  '-Wmissing-declarations',
  '-Wmissing-format-attribute',
  '-Wmissing-include-dirs',
  '-Wmissing-noreturn',
  '-Wnested-externs',
  '-Wno-cast-function-type',
  '-Wno-dangling-pointer',
  '-Wno-missing-field-initializers',
  '-Wno-sign-compare',
  '-Wno-unused-parameter',
  '-Wold-style-definition',
  '-Wpointer-arith',
  '-Wredundant-decls',
  '-Wstrict-prototypes',
  '-Wswitch-default',
  '-Wswitch-enum',
  '-Wundef',
  '-Wuninitialized',
  '-Wunused',
  '-fno-strict-aliasing',
  ['-Werror=format-security', '-Werror=format=2'],
]
if get_option('buildtype') != 'plain'
  test_c_args += '-fstack-protector-strong'
endif
foreach arg: test_c_args
  if cc.has_multi_arguments(arg)
    project_c_args += arg
  endif
endforeach
add_project_arguments(project_c_args, language: 'c')

includes = [
  include_directories('.'),
  include_directories('src')
]

dependencies = [
  dependency('glib-2.0', version : '>=2.16'),
  dependency('mm-glib', version : '>=1.14'),
  dependency('libsoup-3.0'),
  dependency('libcares', version : '>=1.18.1'),
  cc.find_library('dl'),
  cc.find_library('phonenumber', required: true)
]

subdir('src')
subdir('plugins')

mmsd = executable('mmsdtng',
  'src/main.c',
  dependencies : dependencies,
  include_directories : includes,
  install : true,
  link_with : [mms_lib, plugins_lib]
)

subdir('tools')
subdir('unit')
