#!/usr/bin/python

import pydbus
import gi.repository
from pydbus import SessionBus
from gi.repository import GLib
bus = SessionBus()
TestServer = bus.get("org.ofono.mms", "/org/ofono/mms/modemmanager")

options = GLib.Variant('a{sv}', {
    'DeliveryReport': GLib.Variant('b', False),
    'Subject': GLib.Variant('s', "Test Subject!"),
})

TestServer.SendMessage(["+1XXXXXXXXXX"], options, [("cid-1", "text/plain", "/home/mobian/Test-MMS/mmsattachments/text.txt"), ("cid-2", "image/jpeg", "/home/mobian/Test-MMS/mmsattachments/JPG1200x1600.jpg")])

